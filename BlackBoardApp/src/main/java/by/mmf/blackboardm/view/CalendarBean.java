/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.mmf.blackboardm.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.imageio.ImageIO;

/**
 *
 * @author jack
 */
@ManagedBean(name = "calendarBean")
@SessionScoped
public class CalendarBean {

    private String imgPath = "JavaKaffe";

    public String getImgPath() {
        return imgPath;
    }

    public String getRealImgPath() {
        if (!imgPath.equals("JavaKaffe")) {
            return "p2.jpg";
        }
        return imgPath + ".jpg";
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }
}
